import {
	ScrollView,
	View,
	Text,
	Image,
	Modal,
	TextInput,
	AsyncStorage,
	TouchableOpacity,
	ImageBackground
} from 'react-native';
import React, { Component } from 'react';
import Device from 'react-native-device-detection';

import commonStyle from '../../common/style';
import pageStyle from './style';
import images from '../../common/image';

import Header from '../../common/header';
import Footer from '../../common/footer';

import RadioButton from '../../common/button/radio-button';
import SwitchButton from '../../common/button/switch-button';

import levelConfig from '../../common/config/level.js';

let bestScore = {};

class Options extends Component {
	constructor(props) {
		super(props);
		this.state = {
			difficult: 'Easy',
			showCredit: true,
			autoReload: true,
			level: 'level-1',
			showModalConfigLayout: false
		};

		this.openModal = this.openModal.bind(this);
		this.onRequestClose = this.onRequestClose.bind(this);
		this.onAutoReloadChange = this.onAutoReloadChange.bind(this);
		this.onShowCreditModeChange = this.onShowCreditModeChange.bind(this);
	}

	componentWillMount() {
		AsyncStorage.getItem('config').then(data => {
			if (data != null) {
				let config = JSON.parse(data);
				bestScore = config.bestScore;
				this.setState({
					level: config.level,
					difficult: config.difficult,
					showCredit: config.showCredit,
					autoReload: config.autoReload
				});
			}
		});
	}

	saveConfig(option) {
		let config = Object.assign({}, this.getConfig(), option);
		AsyncStorage.setItem('config', JSON.stringify(config));
	}

	getConfig() {
		return {
			level: this.state.level,
			bestScore: bestScore,
			difficult: this.state.difficult,
			autoReload: this.state.autoReload,
			showCredit: this.state.showCredit
		};
	}

	reset() {
		this.setState({
			showCredit: true,
			autoReload: true,
			level: 'level-1',
			difficult: 'Easy',
			showModalConfigLayout: false
		});

		AsyncStorage.removeItem('config').then(() => {
			AsyncStorage.setItem('config', JSON.stringify(this.getConfig()));
		});
	}

	onLeftPress() {
		this.props.navigation.goBack();
	}

	onRightPress() {
		this.props.navigation.navigate('play', {
			config: this.getConfig()
		});
	}

	onShowCreditModeChange() {
		let showCredit = !this.state.showCredit;
		this.setState({
			showCredit: showCredit
		});
		this.saveConfig({ showCredit: showCredit });
	}

	onDifficultModeChange(value) {
		this.setState({
			difficult: value
		});
		this.saveConfig({ difficult: value });
	}

	onAutoReloadChange() {
		let autoReload = !this.state.autoReload;
		this.setState({
			autoReload: autoReload
		});
		this.saveConfig({ autoReload: autoReload });
	}

	openModal() {
		this.setState({
			showModalConfigLayout: true
		});
	}

	onRequestClose() {
		this.setState({
			showModalConfigLayout: false
		});
	}

	selectLevel(level) {
		this.setState({
			level: level,
			showModalConfigLayout: false
		});
		this.saveConfig({ level: level });
	}

	render() {
		const { level, difficult } = this.state;

		return (
			<ImageBackground
				source={images.app.backgroundScreen2}
				style={commonStyle.backgroundImage}
			>
				<Header
					theme="light"
					title="Options"
					rightLabel="Play"
					onLeftPress={() => this.onLeftPress()}
					onRightPress={() => this.onRightPress()}
				/>

				<ScrollView style={commonStyle.container}>
					<Text style={pageStyle.optionLabel}>Show description for image</Text>
					<SwitchButton
						value={this.state.showCredit}
						labelOn="YES"
						labelOff="NO"
						onPress={this.onShowCreditModeChange}
					/>

					<Text style={pageStyle.optionLabel}>Difficult</Text>
					<View style={pageStyle.viewRadioButton}>
						<RadioButton
							label="Easy"
							checked={difficult == 'Easy' ? true : false}
							onPress={() => this.onDifficultModeChange('Easy')}
						/>
						<RadioButton
							label="Medium"
							checked={difficult == 'Medium' ? true : false}
							onPress={() => this.onDifficultModeChange('Medium')}
						/>
						<RadioButton
							label="Hard"
							checked={difficult == 'Hard' ? true : false}
							onPress={() => this.onDifficultModeChange('Hard')}
						/>
					</View>

					<Text style={pageStyle.optionLabel}>Card layout</Text>
					<TouchableOpacity onPress={this.openModal}>
						<View style={pageStyle.coverButton}>
							<Text style={pageStyle.dropdownLabel}>
								{levelConfig[level] ? levelConfig[level].title : ''}
							</Text>
							<Image
								source={images.app.iconArrowDown}
								style={pageStyle.dropdownIcon}
							/>
						</View>
					</TouchableOpacity>

					<TouchableOpacity onPress={() => this.reset()}>
						<ImageBackground
							source={images.app.buttonModal}
							style={pageStyle.buttonImage}
						>
							<Text style={pageStyle.buttonText}>Reset config</Text>
						</ImageBackground>
					</TouchableOpacity>
				</ScrollView>

				<Modal
					animationType={'slide'}
					transparent={true}
					visible={this.state.showModalConfigLayout}
					onRequestClose={this.onRequestClose}
				>
					<View style={pageStyle.modalBackground}>
						<View style={pageStyle.modalContent}>
							<View style={pageStyle.headBar}>
								<TouchableOpacity
									onPress={this.onRequestClose}
									style={pageStyle.closeButton}
								>
									<Image
										source={images.app.iconClose}
										style={pageStyle.closeIcon}
									/>
								</TouchableOpacity>
							</View>

							<View style={pageStyle.modalView}>
								<TouchableOpacity onPress={() => this.selectLevel('level-1')}>
									<Text style={pageStyle.optionText}>
										{levelConfig['level-1'].title}
									</Text>
								</TouchableOpacity>

								<TouchableOpacity onPress={() => this.selectLevel('level-2')}>
									<Text style={pageStyle.optionText}>
										{levelConfig['level-2'].title}
									</Text>
								</TouchableOpacity>

								<TouchableOpacity onPress={() => this.selectLevel('level-3')}>
									<Text style={pageStyle.optionText}>
										{levelConfig['level-3'].title}
									</Text>
								</TouchableOpacity>

								<TouchableOpacity onPress={() => this.selectLevel('level-4')}>
									<Text style={pageStyle.optionText}>
										{levelConfig['level-4'].title}
									</Text>
								</TouchableOpacity>

								<TouchableOpacity onPress={() => this.selectLevel('level-5')}>
									<Text style={pageStyle.optionText}>
										{levelConfig['level-5'].title}
									</Text>
								</TouchableOpacity>
							</View>
						</View>
					</View>
				</Modal>

				<Footer theme="white" />
			</ImageBackground>
		);
	}
}

export default Options;
