import { Platform, StyleSheet, Dimensions } from 'react-native';
import colors from '../../common/style/color';

const { width } = Dimensions.get('window');
const modalContentWidth = Math.min(width - 60, 300);

const pageStyle = StyleSheet.create({
	optionLabel: {
		fontFamily: 'GothamRounded-Bold',
		color: colors.white,
		fontSize: 20,
		marginTop: 30,
		marginBottom: 10,
		backgroundColor: 'transparent'
	},
	viewRadioButton: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	levelConfig: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: 'transparent'
	},
	coverButton: {
		justifyContent: 'space-between',
		backgroundColor: '#4f8d32',
		flexDirection: 'row',
		alignItems: 'center',
		width: 250,
		height: 40,
		paddingLeft: 10,
		paddingRight: 10,
		borderRadius: 20
	},
	dropdownLabel: {
		paddingLeft: 15,
		paddingTop: Platform.OS === 'ios' ? 4 : 0,
		fontFamily: 'GothamRounded-Bold',
		color: colors.white
	},
	dropdownIcon: {
		height: 10,
		resizeMode: 'contain'
	},
	buttonImage: {
		marginBottom: 20,
		marginTop: 20,
		height: 50,
		width: 200,
		paddingLeft: 30,
		justifyContent: 'center'
	},
	buttonText: {
		backgroundColor: 'transparent',
		color: colors.white,
		paddingTop: Platform.OS === 'ios' ? 4 : 0,
		fontFamily: 'GothamRounded-Bold'
	},
	modalBackground: {
		backgroundColor: 'rgba(0,0,0,0.5)',
		alignItems: 'center',
		flex: 1,
		justifyContent: 'center'
	},
	modalContent: {
		backgroundColor: colors.whiteSmoke,
		width: modalContentWidth,
		flexDirection: 'column',
		borderRadius: 10
	},
	modalView: {
		paddingLeft: 15,
		paddingRight: 15,
		paddingBottom: 15
	},
	headBar: {
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	closeButton: {
		paddingRight: 10,
		width: 60,
		height: 30,
		alignItems: 'flex-end',
		justifyContent: 'center'
	},
	closeIcon: {
		width: 10,
		height: 10
	},
	optionText: {
		color: colors.black,
		padding: 10,
		fontFamily: 'GothamRounded-Bold'
	}
});

export default pageStyle;
