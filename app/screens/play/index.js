import {
	View,
	Image,
	Text,
	Modal,
	Linking,
	Platform,
	ScrollView,
	BackHandler,
	AsyncStorage,
	TouchableOpacity,
	ImageBackground
} from 'react-native';
import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';

import pageStyle from './style';
import Card from './card';

import images from '../../common/image';
import Header from '../../common/header';
import Footer from '../../common/footer';
import commonStyle from '../../common/style';
import levelConfig from '../../common/config/level.js';
import bestScore from '../../common/config/best-score.js';
import PopupCredit from './popup/popupCredit';
import PopupWining from './popup/popupWining';
import PopupLosing from './popup/popupLosing';
import PopupExit from './popup/popupExit';
import PopupGoBack from './popup/popupGoBack';

let allConfig = {};
let listener = null;

let layout = {
	difficult: 'Easy',
	level: '',
	data: [],
	size: 8,
	time: 120,
	moves: 16,
	config: [],
	bestScore: {},
	showCredit: true,
	autoReload: true
};
let riddles = images.riddles;

class Play extends Component {
	constructor(props, context) {
		super(props, context);

		this.state = {
			sourceCards: [], // cards for play;
			openCards: 0, // total cards have been opened;
			bestTime: 0,
			bestMove: 0,
			moves: 0,
			credit: {}, // credit;
			timer: null,
			waiting: true,
			duration: 0,
			selected: null, // card have just selected;
			showExitModal: false,
			showCreditModal: false,
			showWinningModal: false,
			showLosingModal: false,
			showModalConfirmBackToHome: false
		};

		this.setHighestScore = this.setHighestScore.bind(this);
		this.openLink = this.openLink.bind(this);
		this.openCard = this.openCard.bind(this);
		this.replay = this.replay.bind(this);
		this.quitApp = this.quitApp.bind(this);
		this.confirmToQuit = this.confirmToQuit.bind(this);
		this.onExitModalClose = this.onExitModalClose.bind(this);
		this.hardwareBackPress = this.hardwareBackPress.bind(this);
		this.onLosingModalClose = this.onLosingModalClose.bind(this);
		this.onWinningModalClose = this.onWinningModalClose.bind(this);
	}

	componentWillMount() {
		AsyncStorage.getItem('config').then(data => {
			if (data != null) {
				// console.log('data:', data);
				let config = JSON.parse(data);
				let level = levelConfig[config.level];

				layout.difficult = config.difficult;

				layout.showCredit = config.showCredit;
				layout.autoReload = config.autoReload;

				layout.level = config.level;
				layout.size = level.size;
				layout.time = level.time;
				layout.moves = level.moves;
				layout.config = level.layout;

				//get best time, best move from store;
				let scoreId = config.difficult + ':' + config.level;
				// init default bestScore;
				if (!config.bestScore) {
					config.bestScore = bestScore;
				}
				allConfig = Object.assign({}, config); // store option configs;
				layout.bestScore = config.bestScore[scoreId];
			} else {
				// set default;
				layout.difficult = 'Easy';
				layout.showCredit = true;
				layout.autoReload = true;
				layout.size = 8;
				layout.moves = 16;
				layout.time = 120;
				layout.level = 'level-1';
				layout.config = levelConfig['level-1'].layout;
				layout.bestScore = {
					time: 999,
					move: 999
				};
			}
			this.setupLayout();
		});
	}

	reset() {
		this.setState({
			showCredit: true,
			autoReload: true,
			level: 'level-1'
		});
	}

	saveConfig(option) {
		AsyncStorage.setItem('config', JSON.stringify(allConfig));
	}

	setHighestScore() {
		const { duration, moves } = this.state;
		const scoreId = layout.difficult + ':' + layout.level;
		let bestScore = layout.bestScore;
		// get smallest moves;
		if (bestScore.move > moves) {
			bestScore.move = moves;
		}

		// get smallest time;
		if (bestScore.time > duration) {
			bestScore.time = duration;
		}

		this.setState({
			bestMove: bestScore.move,
			bestTime: bestScore.time
		});

		allConfig.bestScore[scoreId] = bestScore;
		this.saveConfig();
	}

	setupLayout() {
		const { size, bestScore } = layout;
		const length = riddles.length;
		let sourceCards = [];
		let card = {};
		let i = 0;
		let mapCards = {};

		while (i < size) {
			let randomIndex = Math.floor(Math.random() * length);
			if (mapCards['i-' + randomIndex]) continue;
			mapCards['i-' + randomIndex] = true;
			if (i % 2 == 0) {
				// pick random card from riddeles;
				card = riddles[randomIndex];
			}
			sourceCards.push({ ...card, opened: false, display: false, cardIndex: i });
			i += 1;
		}

		// shuffle cards;
		for (let ii = 0; ii < 4; ++ii) {
			for (let i = 0; i < size; ++i) {
				let randomIndex = Math.floor(Math.random() * size);
				let randomIndex2 = Math.floor(Math.random() * size);
				let randomCard = sourceCards[randomIndex];
				sourceCards[randomIndex] = sourceCards[i];
				sourceCards[i] = sourceCards[randomIndex2];
				sourceCards[randomIndex2] = randomCard;
			}
		}
		// console.log('sourceCards', sourceCards);
		this.setState({
			sourceCards: sourceCards,
			openCards: 0,
			duration: 0,
			moves: 0,
			timer: null,
			waiting: false,
			selected: null,
			bestTime: bestScore.time,
			bestMove: bestScore.move
		});
	}

	// android requires onRequestClose;
	onWinningModalClose() {
		this.setState({
			showWinningModal: false
		});
	}

	onLosingModalClose() {
		this.setState({
			showLosingModal: false
		});
	}

	onCreditModalClose() {
		this.startWatch();
		this.setState({
			showCreditModal: false
		});
	}

	onExitModalClose() {
		this.stopWatch();
		this.setState({
			showExitModal: false
		});
	}

	onCloseModalConfirmBackToHome() {
		this.setState({
			showModalConfirmBackToHome: false
		});
	}

	backToHomeScreen() {
		this.onCloseModalConfirmBackToHome();
		this.stopWatch();

		setTimeout(function () {
			const resetAction = NavigationActions.reset({
				index: 0,
				actions: [NavigationActions.navigate({ routeName: 'home' })]
			});
			this.props.navigation.dispatch(resetAction);
		}.bind(this), 345);
	}

	confirmToQuit() {
		this.stopWatch();
		this.setState({
			showExitModal: true,
			showLosingModal: false,
			showWinningModal: false
		});
	}

	hardwareBackPress() {
		this.openModalConfirmBackToHome();
		return true;
	}

	componentDidMount() {
		BackHandler.addEventListener("hardwareBackPress", this.hardwareBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener("hardwareBackPress", this.hardwareBackPress);
	}

	quitApp() {
		this.onExitModalClose();
		if (Platform.OS === 'android') {
			return BackHandler.exitApp();
		}
	}

	openModalConfirmBackToHome() {
		this.setState({
			showModalConfirmBackToHome: true
		});
	}

	replay() {
		this.setupLayout();
		this.onLosingModalClose();
		this.onWinningModalClose();
	}

	openLink(creditName) {
		Linking.openURL('https://www.google.com/search?q=' + creditName);
	}

	openCard(index) {
		let sourceCards = this.state.sourceCards;
		sourceCards[index].display = true;

		this.setState({
			sourceCards: sourceCards
		});

		if (sourceCards[index].opened == true) {
			if (layout.showCredit) {
				this.state.credit = sourceCards[index];
				this.setState({
					showCreditModal: true
				});
				this.stopWatch();
			}
		} else {
			setTimeout(() => this.checkCard(index), 345);
		}
	}

	checkCard(index) {
		let sourceCards = this.state.sourceCards;
		let selected = this.state.selected;
		let change = this.state.change;
		let openCards = this.state.openCards;

		if (sourceCards[selected] == null) {
			this.state.moves += 1;
			// init state;
			this.setState({
				selected: index
			});
		} else if (sourceCards[selected].cardIndex != sourceCards[index].cardIndex) {
			// ignore case click on the opened card again.
			// reset state;
			if (sourceCards[selected].id == sourceCards[index].id) {
				openCards += 2;
				// display won modal;
				if (sourceCards.length == openCards) {
					this.stopWatch();
					this.setHighestScore();
					this.state.showWinningModal = true;
				} else if (layout.showCredit) {
					this.stopWatch();
					this.state.moves += 1;
					this.state.showCreditModal = true;
					this.state.credit = sourceCards[index];
				}

				sourceCards[index].opened = true;
				sourceCards[selected].opened = true;
			} else {
				this.state.moves += 1;
				sourceCards[index].display = false;
				sourceCards[selected].display = false;
			}

			this.setState({
				change: ++change,
				selected: null,
				openCards: openCards,
				sourceCards: sourceCards
			});
		}

		if (layout.difficult == 'Medium' || layout.difficult == 'Hard') {
			if (this.state.moves >= layout.moves && !this.state.showWinningModal) {
				this.setState({
					showLosingModal: true
				});
				this.stopWatch();
			}
		}

		// init time start for counting duration;
		if (!this.state.duration) {
			this.startWatch();
		}
	}

	startWatch() {
		this.state.duration += 1;
		this.forceUpdate();
		this.state.timer = setTimeout(() => this.startWatch(), 1000);
		if (layout.difficult == 'Hard' && this.state.duration >= layout.time) {
			this.setState({
				showLosingModal: true
			});
			this.stopWatch();
		}
	}

	stopWatch() {
		clearTimeout(this.state.timer);
	}

	nextLevel() {
		const allLevel = [
			'Easy:level-1',
			'Easy:level-2',
			'Easy:level-3',
			'Easy:level-4',
			'Easy:level-5',
			'Medium:level-1',
			'Medium:level-2',
			'Medium:level-3',
			'Medium:level-4',
			'Medium:level-5',
			'Hard:level-1',
			'Hard:level-2',
			'Hard:level-3',
			'Hard:level-4',
			'Hard:level-5'
		];
		const current = layout.difficult + ':' + layout.level;
		let newLevel = null;
		for (let i = 0; i < allLevel.length; ++i) {
			if (current == allLevel[i]) {
				newLevel = allLevel[i + 1];
				break;
			}
		}

		if (newLevel) {
			let newConfig = newLevel.split(':');
			layout.difficult = newConfig[0];
			layout.level = newConfig[1];

			let level = levelConfig[layout.level];
			layout.size = level.size;
			layout.time = level.time;
			layout.moves = level.moves;
			layout.config = level.layout;
			layout.bestScore = allConfig.bestScore[newLevel];
			allConfig.level = layout.level;
			allConfig.difficult = layout.difficult;
			this.saveConfig();
			this.setupLayout();
			this.onWinningModalClose();
		}
	}

	renderCard() {
		const sourceCards = this.state.sourceCards;
		const gridCards = layout.config;
		const rows = gridCards.length;

		let cardLayout = [];
		let cardIndex = 0;

		for (let i = 0; i < rows; ++i) {
			let columns = gridCards[i].length;
			let cardRow = [];

			for (let j = 0; j < columns; ++j) {
				if (gridCards[i][j] == 0) {
					cardRow.push(<Card key={i + '-' + j} />);
				} else {
					let cardItem = sourceCards[cardIndex];
					let index = cardIndex;
					cardRow.push(
						<Card
							source={cardItem.display === true ? cardItem.source : 0}
							key={index}
							onPress={() => this.openCard(index)}
						/>
					);
					cardIndex += 1;
				}
			}

			cardLayout.push(
				<View style={pageStyle.rowCard} key={i}>
					{cardRow}
				</View>
			);
		}
		return cardLayout;
	}

	textModeDifficult() {
		let timeLimit = layout.time - this.state.duration;
		let movesLimit = layout.moves - this.state.moves;
		if (layout.difficult == 'Medium') {
			return (
				<View style={pageStyle.logging}>
					<View style={pageStyle.scoreLog}>
						<Text style={pageStyle.textScore}>Time: {this.state.duration}s</Text>
					</View>
					<View style={pageStyle.scoreLog}>
						<Text style={pageStyle.textScore}>Moves: </Text>
						<Text
							style={
								movesLimit <= 5 ? pageStyle.textScoreWarning : pageStyle.textScore
							}
						>
							{movesLimit}
						</Text>
					</View>
				</View>
			);

		} else if (layout.difficult == 'Easy') {
			return (
				<View style={pageStyle.scoreLog}>
					<Text style={pageStyle.textScore}>Time: {this.state.duration}s</Text>
					<Text style={pageStyle.textScore}>Moves: {this.state.moves}</Text>
				</View>
			);
		} else {
			return (
				<View style={pageStyle.logging}>
					<View style={pageStyle.scoreLog}>
						<Text style={pageStyle.textScore}>Time: </Text>
						<Text
							style={
								timeLimit <= 5 ? pageStyle.textScoreWarning : pageStyle.textScore
							}
						>
							{timeLimit}s
						</Text>
					</View>
					<View style={pageStyle.scoreLog}>
						<Text style={pageStyle.textScore}>Moves: </Text>
						<Text
							style={
								movesLimit <= 5 ? pageStyle.textScoreWarning : pageStyle.textScore
							}
						>
							{movesLimit}
						</Text>
					</View>
				</View>
			);
		}
	}

	render() {
		let { credit, bestMove, bestTime } = this.state;

		return (
			<ImageBackground
				source={images.app.backgroundScreen2}
				style={commonStyle.backgroundImage}
			>
				<Header
					theme="light"
					title={layout.difficult}
					rightLabel="Exit"
					onLeftPress={() => this.openModalConfirmBackToHome()}
					onRightPress={Platform.OS === 'android' ? () => this.confirmToQuit() : null}
				/>

				{this.state.waiting ? null : (
					<View style={pageStyle.playground}>
						<View>
							<View style={pageStyle.scoreLog}>
								<Text style={pageStyle.textScore}>
									Best time: {bestTime > 998 ? '' : bestTime}
								</Text>
								<Text style={pageStyle.textScore}>
									Best moves: {bestMove > 998 ? '' : bestMove}
								</Text>
							</View>
							{this.renderCard()}
							{this.textModeDifficult()}
						</View>
					</View>
				)}

				<PopupCredit
					open={this.state.showCreditModal}
					close={() => this.onCreditModalClose()}
					credit={credit}
					openLink={() => this.openLink(credit.name)}
				/>

				<PopupWining
					confirmToQuit={() => this.confirmToQuit()}
					open={this.state.showWinningModal}
					close={() => this.onWinningModalClose()}
					replay={() => this.replay()}
					nextLevel={() => this.nextLevel()}
					moveTaken={this.state.moves}
					timeTaken={this.state.duration}
				/>

				<PopupLosing
					open={this.state.showLosingModal}
					ok={() => this.backToHomeScreen()}
					close={() => this.onLosingModalClose()}
					replay={() => this.replay()}
				/>

				<PopupExit
					open={this.state.showExitModal}
					ok={() => this.quitApp()}
					close={() => this.onExitModalClose()}
				/>

				<PopupGoBack
					open={this.state.showModalConfirmBackToHome}
					ok={() => this.backToHomeScreen()}
					close={() => this.onCloseModalConfirmBackToHome()}
				/>

				<Footer theme="white" />

			</ImageBackground>
		);
	}
}

export default Play;
