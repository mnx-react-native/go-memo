import React from 'react';
import { Modal, View, TouchableOpacity, Text, Image, ImageBackground } from 'react-native';
import images from '../../../common/image';
import PropTypes from 'prop-types';
import pageStyle from '../style';

const PopupLosing = props => {
	return (
		<Modal
			animationType="slide"
			transparent={true}
			visible={props.open}
			onRequestClose={props.close}
		>
			<View style={pageStyle.modalBackground}>
				<View style={[pageStyle.modalContent, { alignItems: 'center' }]}>
					<Text style={[pageStyle.wonText, { paddingTop: 20 }]}>YOU LOST</Text>

					<TouchableOpacity onPress={props.ok}>
						<Text style={pageStyle.textBold}>BACK TO MENU</Text>
					</TouchableOpacity>

					<TouchableOpacity onPress={props.replay}>
						<ImageBackground
							source={images.app.buttonModal}
							style={pageStyle.buttonImage}
						>
							<Text style={pageStyle.buttonText}>REPLAY</Text>
						</ImageBackground>
					</TouchableOpacity>
				</View>
			</View>
		</Modal>
	);
}

PopupLosing.propTypes = {
	ok: PropTypes.func,
	open: PropTypes.bool,
	close: PropTypes.func,
	replay: PropTypes.func
};

export default PopupLosing;
