import React from 'react';
import { Modal, View, TouchableOpacity, Text, Image } from 'react-native';
import images from '../../../common/image';
import PropTypes from 'prop-types';
import pageStyle from '../style';

const PopupCredit = props => {
	return (
		<Modal
			animationType="slide"
			transparent={true}
			visible={props.open}
			onRequestClose={props.close}
		>
			<View style={pageStyle.modalBackground}>
				<View style={pageStyle.modalContent}>
					<View style={pageStyle.headBar}>
						<TouchableOpacity
							onPress={props.close}
							style={pageStyle.closeButton}
						>
							<Image source={images.app.iconClose} style={pageStyle.closeIcon} />
						</TouchableOpacity>
					</View>

					<View style={pageStyle.modalView}>
						<Image source={props.credit.source} style={pageStyle.thumb} />

						<View style={pageStyle.modalInfo}>
							<Text style={pageStyle.textLabel}>Name: </Text>
							<Text style={pageStyle.textValue}>{props.credit.name}</Text>
						</View>
						<View style={pageStyle.modalInfo}>
							<Text style={pageStyle.textLabel}>Agency: </Text>
							<Text style={pageStyle.textValue}>{props.credit.agency}</Text>
						</View>
						<View style={pageStyle.modalInfo}>
							<Text style={pageStyle.textLabel}>Credit: </Text>
							<Text style={pageStyle.textValue}>{props.credit.credit}</Text>
						</View>
						<View style={pageStyle.modalInfo}>
							<Text style={pageStyle.textLabel}>Location: </Text>
							<Text style={pageStyle.textValue}>
								{props.credit.location}
							</Text>
						</View>
						<TouchableOpacity onPress={props.openLink} active={0.9}>
							<View style={pageStyle.buttonMore}>
								<Text style={pageStyle.buttonText}>Know more</Text>
							</View>
						</TouchableOpacity>
						<TouchableOpacity onPress={props.close} active={0.9}>
							<View style={[pageStyle.buttonMore, { marginBottom: 10 }]}>
								<Text style={pageStyle.buttonText}>OK</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		</Modal>
	);
}

PopupCredit.propTypes = {
	close: PropTypes.func,
	open: PropTypes.bool,
	credit: PropTypes.object,
	openLink: PropTypes.func
};

export default PopupCredit;
