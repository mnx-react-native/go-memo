import React from 'react';
import { Modal, View, TouchableOpacity, Text } from 'react-native';
import images from '../../../common/image';
import PropTypes from 'prop-types';
import pageStyle from '../style';

const PopupExit = props => {

	return (
		<Modal
			animationType="slide"
			transparent={true}
			visible={props.open}
			onRequestClose={props.close}
		>
			<View style={pageStyle.modalBackground}>
				<View style={[pageStyle.modalContent, pageStyle.modalView]}>
					<Text style={[pageStyle.textBold, { paddingTop: 20 }]}>
						Do you really want to quit?
						</Text>
					<View style={pageStyle.groupButton}>
						<TouchableOpacity onPress={props.close}>
							<View style={pageStyle.buttonConfirmNo}>
								<Text style={pageStyle.buttonText}>NO</Text>
							</View>
						</TouchableOpacity>

						<TouchableOpacity onPress={props.ok}>
							<View style={pageStyle.buttonConfirmYes}>
								<Text style={pageStyle.buttonText}>YES</Text>
							</View>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		</Modal>
	);

}

PopupExit.propTypes = {
	ok: PropTypes.func,
	open: PropTypes.bool,
	close: PropTypes.func
};

export default PopupExit;
