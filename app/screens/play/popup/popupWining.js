import React from 'react';
import { Modal, View, TouchableOpacity, Text, Image, ImageBackground } from 'react-native';
import images from '../../../common/image';
import PropTypes from 'prop-types';
import pageStyle from '../style';

const PopupWining = props => {
	return (
		<Modal
			animationType="slide"
			transparent={true}
			visible={props.open}
			onRequestClose={props.close}
		>
			<View style={pageStyle.modalBackground}>
				<View style={[pageStyle.modalContent, { alignItems: 'center' }]}>
					<Text style={[pageStyle.textBold, { paddingTop: 20 }]}>
						Congratulations!
					</Text>

					<Text style={pageStyle.wonText}>YOU WON</Text>

					<Text style={pageStyle.textBold}>
						Time taken: {props.timeTaken}s
						</Text>
					<Text style={pageStyle.textBold}>
						Moves taken: {props.moveTaken}
					</Text>
					<TouchableOpacity onPress={props.confirmToQuit}>
						<ImageBackground
							source={images.app.buttonModal}
							style={pageStyle.buttonImage}
						>
							<Text style={pageStyle.buttonText}>Exit</Text>
						</ImageBackground>
					</TouchableOpacity>

					<TouchableOpacity onPress={props.replay}>
						<ImageBackground
							source={images.app.buttonModal}
							style={pageStyle.buttonImage}
						>
							<Text style={pageStyle.buttonText}>Replay</Text>
						</ImageBackground>
					</TouchableOpacity>

					<TouchableOpacity onPress={props.nextLevel}>
						<ImageBackground
							source={images.app.buttonModal}
							style={pageStyle.buttonImage}
						>
							<Text style={pageStyle.buttonText}>Next Level</Text>
						</ImageBackground>
					</TouchableOpacity>
				</View>
			</View>
		</Modal>
	);
}

PopupWining.propTypes = {
	open: PropTypes.bool,
	close: PropTypes.func,
	replay: PropTypes.func,
	nextLevel: PropTypes.func,
	confirmToQuit: PropTypes.func,
	timeTaken: PropTypes.number,
	moveTaken: PropTypes.number
};

export default PopupWining;
