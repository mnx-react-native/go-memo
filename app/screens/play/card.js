import { View, Image, Text, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import Device from 'react-native-device-detection';
import React from 'react';
import PropTypes from 'prop-types';
import images from '../../common/image';

const { width } = Dimensions.get('window');
const cardWidth = Device.isPhone ? (width - 48) / 4 : (width - 52) / 6;

const pageStyle = StyleSheet.create({
	card: {
		width: cardWidth,
		margin: 1,
		height: cardWidth,
		borderRadius: 10
	}
});

const Card = props => {
	let cardSource = '';
	if (props.onPress) {
		cardSource = (
			<TouchableOpacity onPress={props.onPress} activeOpacity={0.9}>
				<Image source={props.source || images.app.backgroundCard} style={pageStyle.card} />
			</TouchableOpacity>
		);
	} else {
		cardSource = (<View style={pageStyle.card} />);
	}
	return cardSource;
};

Card.propTypes = {
	source: PropTypes.number,
	onPress: PropTypes.func
};

export default Card;
