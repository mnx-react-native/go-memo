import { Platform, StyleSheet, Dimensions } from 'react-native';
import colors from '../../common/style/color';

const { width } = Dimensions.get('window');
const modalContentWidth = Math.min(width - 60, 300);
const thumbWidth = modalContentWidth - 30;
const thumbHeight = Math.round(thumbWidth * 0.6);

const buttonConfirm = {
	borderRadius: 20,
	width: 80,
	height: 40,
	alignItems: 'center',
	justifyContent: 'center'
};

const pageStyle = StyleSheet.create({
	playground: {
		flexDirection: 'column',
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	rowCard: {
		flexDirection: 'row',
		justifyContent: 'center'
	},
	logging: {
		paddingTop: 5,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	scoreLog: {
		justifyContent: 'space-between',
		flexDirection: 'row',
		backgroundColor: 'transparent',
		alignItems: 'flex-end',
		margin: 5
	},
	timeLog: {
		alignItems: 'flex-start',
		flexDirection: 'column'
	},
	textScore: {
		color: colors.white,
		fontFamily: 'GothamRounded-Bold'
	},
	textScoreWarning: {
		color: colors.red,
		fontFamily: 'GothamRounded-Bold'
	},
	modalBackground: {
		backgroundColor: 'rgba(0,0,0,0.5)',
		alignItems: 'center',
		flex: 1,
		justifyContent: 'center'
	},
	modalContent: {
		backgroundColor: colors.whiteSmoke,
		width: modalContentWidth,
		flexDirection: 'column',
		borderRadius: 10
	},
	modalView: {
		paddingLeft: 15,
		paddingRight: 15,
		paddingBottom: 15
	},
	modalConfirm: {
		backgroundColor: colors.whiteSmoke,
		borderRadius: 10,
		padding: 15
	},
	headBar: {
		paddingBottom: 5,
		paddingTop: 5,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	closeButton: {
		paddingRight: 10,
		width: 60,
		height: 30,
		alignItems: 'flex-end',
		justifyContent: 'center'
	},
	closeIcon: {
		width: 10,
		height: 10
	},
	wonText: {
		color: colors.blue,
		fontSize: 32,
		padding: 10,
		textAlign: 'center',
		fontFamily: 'GothamRounded-Bold'
	},
	thumb: {
		borderRadius: 5,
		width: thumbWidth,
		height: thumbHeight,
		marginBottom: 10
	},
	buttonImage: {
		marginBottom: 20,
		height: 50,
		width: 200,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: 'transparent'
	},
	groupButton: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	buttonConfirmNo: {
		backgroundColor: '#ecb622',
		...buttonConfirm
	},
	buttonConfirmYes: {
		backgroundColor: '#67a847',
		...buttonConfirm
	},
	buttonMore: {
		backgroundColor: '#67a847',
		marginTop: 20,

		padding: 7,
		borderRadius: 20,
		alignItems: 'center'
	},
	buttonText: {
		paddingTop: Platform.OS == 'ios' ? 4 : 0,
		color: colors.white,
		fontWeight: 'bold',
		fontSize: 16,
		fontFamily: 'GothamRounded-Bold'
	},
	modalInfo: {
		paddingTop: 15,
		flexDirection: 'row'
	},
	textBold: {
		paddingBottom: 10,
		fontSize: 16,
		textAlign: 'center',
		fontWeight: 'bold',
		fontFamily: 'GothamRounded-Bold'
	},
	textLabel: {
		fontSize: 16,
		fontFamily: 'GothamRounded-Medium'
	},
	textValue: {
		flexWrap: 'wrap',
		flex: 1,
		color: '#05C0FE',
		fontSize: 16,
		fontFamily: 'GothamRounded-Medium'
	}
});

export default pageStyle;
