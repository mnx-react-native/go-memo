import {
	ScrollView,
	View,
	Text,
	Image,
	TouchableOpacity,
	AsyncStorage,
	ImageBackground
} from 'react-native';
import React, { Component } from 'react';
import Footer from '../../common/footer';

import MenuButton from './button';
import pageStyle from './style';
import images from '../../common/image';
import commonStyle from '../../common/style';
import bestScore from '../../common/config/best-score.js';

class Home extends Component {
	constructor(props, context) {
		super(props, context);

		this.jumpTo = this.jumpTo.bind(this);
	}

	jumpTo(page) {
		this.props.navigation.navigate(page);
	}

	componentWillMount() {
		AsyncStorage.getItem('config').then(data => {
			// set default config at first time;
			if (!data) {
				let config = {
					level: 'level-1',
					difficult: 'Easy',
					autoReload: true,
					showCredit: true,
					bestScore: bestScore
				};
				AsyncStorage.setItem('config', JSON.stringify(config));
			}
		});
	}

	render() {
		return (
			<ImageBackground
				source={images.app.backgroundScreen}
				style={commonStyle.backgroundImage}
			>
				<View style={[pageStyle.section]}>
					<MenuButton
						onPress={() => this.jumpTo('play')}
						// iconName="iconPlay"
						label="Play"
					/>
					<MenuButton
						onPress={() => this.jumpTo('option')}
						// iconName="iconOption"
						label="Options"
					/>
					<MenuButton
						onPress={() => this.jumpTo('credit')}
						// iconName="iconScore"
						label="Picture Credits"
					/>
					<MenuButton
						onPress={() => this.jumpTo('aboutUs')}
						// iconName="iconAbout"
						label="About Us"
					/>
				</View>

				<Footer theme="dark" />
			</ImageBackground>
		);
	}
}

export default Home;
