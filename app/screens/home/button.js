import {
	View,
	Text,
	Image,
	StyleSheet,
	Platform,
	Dimensions,
	TouchableOpacity,
	ImageBackground
} from 'react-native';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import images from '../../common/image';
import colors from '../../common/style/color';

const { width, height } = Dimensions.get('window');
const buttonWidth = Math.min(width - 100, 300);
const buttonHeight = Math.round(buttonWidth / 5.5);

const marginbutton = height < 500 ? 5 : 20;

const pageStyle = StyleSheet.create({
	buttonMenu: {
		height: buttonHeight,
		width: buttonWidth,
		// resizeMode: 'cover',
		alignItems: 'center',
		flexDirection: 'row',
		marginBottom: marginbutton,
		justifyContent: 'center'
	},
	icon: {
		width: 40,
		height: 40,
		marginLeft: 30
	},
	label: {
		fontFamily: 'GothamRounded-Medium',
		color: colors.white,
		fontSize: 18,
		textAlign: 'center',
		fontWeight: '100',
		backgroundColor: 'transparent',
		paddingTop: Platform.OS === 'ios' ? 4 : 0
	}
});

const MenuButton = props => {
	return (
		<TouchableOpacity onPress={props.onPress}>
			<ImageBackground source={images.app.buttonMenu} style={pageStyle.buttonMenu}>
				<Text style={pageStyle.label}> {props.label.toUpperCase()} </Text>
			</ImageBackground>
		</TouchableOpacity>
	);
};

MenuButton.propTypes = {
	label: PropTypes.string,
	onPress: PropTypes.func,
	iconName: PropTypes.string
};

export default MenuButton;
