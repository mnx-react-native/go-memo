import { Platform, StyleSheet } from 'react-native';
import colors from '../../common/style/color';

const pageStyle = StyleSheet.create({
	header: {
		alignItems: 'center',
		paddingTop: 15
	},
	iconGame: {
		width: 255,
		height: 60
	},
	section: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'flex-end',
		backgroundColor: 'transparent'
	},
	appTitle: {
		fontFamily: 'GothamRounded-Bold',
		fontSize: 20,
		textAlign: 'center',
		backgroundColor: 'transparent',
		color: colors.whiteSmoke
	},
	corverBrand: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center'
	},
	brandIcon: {
		height: 110
	}
});

export default pageStyle;
