import { StackNavigator, NavigationActions } from 'react-navigation';
import { StatusBar, AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import React, { Component } from 'react';
import appStore from './store';

import Option from '../option';
import Home from '../home';
import Play from '../play';
import Credit from '../credit';
import AboutUs from '../about';

const Router = StackNavigator(
	{
		home: {
			screen: Home
		},
		play: {
			screen: Play
		},
		credit: {
			screen: Credit
		},
		option: {
			screen: Option
		},
		aboutUs: {
			screen: AboutUs
		}
	},
	{
		navigationOptions: {
			header: null
		}
	}
);

StatusBar.setHidden(true);

class Main extends Component {
	render() {
		return (
			<Provider store={appStore}>
				<Router />
			</Provider>
		);
	}
}

export default Main;
