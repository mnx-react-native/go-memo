export default {
	increase() {
		return {
			type: 'INCREMENT'
		};
	},

	decrease() {
		return {
			type: 'DECREMENT'
		};
	}
};
