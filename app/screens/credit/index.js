import { ListView, View, Text, Image, TouchableOpacity, ImageBackground } from 'react-native';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import commonStyle from '../../common/style';
import pageStyle from './style';
import images from '../../common/image';

import ListItem from './list-item';
import Header from '../../common/header';
import Footer from '../../common/footer';

import countAction from '../root/action';

class Credit extends Component {
	constructor(props, context) {
		super(props, context);
		const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
		this.state = {
			dataSource: ds.cloneWithRows(images.riddles)
		};
	}

	renderRow(rowData) {
		return (
			<ListItem photo={rowData.source} name={rowData.name} photographer={rowData.credit} />
		);
	}

	onLeftPress() {
		this.props.navigation.goBack();
	}

	render() {
		return (
			<ImageBackground
				source={images.app.backgroundScreen2}
				style={commonStyle.backgroundImage}
			>
				<Header
					theme="light"
					title="Picture Credits"
					onLeftPress={() => this.onLeftPress()}
				/>

				<ListView
					style={commonStyle.container}
					renderRow={this.renderRow}
					dataSource={this.state.dataSource}
				/>

				<Footer theme="white" />
			</ImageBackground>
		);
	}
}

const mapStateToProps = (fullState, thisProps) => {
	return {
		countValue: fullState.count
	};
};

const mapDispatchToProps = dispatch => {
	return {
		doIncrese: () => {
			dispatch(countAction.increase());
		},
		doDecrease: () => {
			dispatch(countAction.decrease());
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Credit);
