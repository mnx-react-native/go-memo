import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';

import colors from '../../common/style/color';
import images from '../../common/image';

const pageStyle = StyleSheet.create({
	listItem: {
		padding: 15,
		flexDirection: 'column',
		margin: 10,
		backgroundColor: colors.white,
		borderRadius: 10
	},
	photo: {
		width: null,
		borderRadius: 5
	},
	textName: {
		paddingTop: 15,
		paddingBottom: 10,
		fontFamily: 'GothamRounded-Bold'
	},
	textScore: {
		color: '#05C0FE',
		fontFamily: 'GothamRounded-Bold'
	}
});

const ListItem = props => {
	return (
		<View style={pageStyle.listItem}>
			<Image style={pageStyle.photo} source={props.photo} />
			<Text style={pageStyle.textName}>{props.name}</Text>
			<Text style={pageStyle.textScore}>PC: {props.photographer}</Text>
		</View>
	);
};
ListItem.propTypes = {
	name: PropTypes.string,
	photo: PropTypes.number,
	photographer: PropTypes.string
};
export default ListItem;
