import { StyleSheet, Platform, Dimensions } from 'react-native';
import colors from '../../common/style/color';
const { width } = Dimensions.get('window');
const thumbWidth = Math.min(width - 60, 300);
const thumbHeight = Math.round(thumbWidth * 0.6);

const pageStyle = StyleSheet.create({
	content: {
		backgroundColor: colors.white,
		flex: 1,
		padding: 10,
		marginTop: 5,
		borderRadius: 5
	},
	header: {
		textAlign: 'center',
		fontSize: 22,
		paddingTop: 15,
		paddingBottom: 15,
		fontFamily: 'GothamRounded-Bold'
	},
	title: {
		textAlign: 'center',
		fontSize: 18,
		paddingBottom: 15,
		fontFamily: 'GothamRounded-Bold'
	},
	textDescription: {
		fontFamily: 'GothamRounded-Medium'
	},
	photoInfo: {
		paddingTop: 15,
		flexDirection: 'row'
	},
	photoCover: {
		marginTop: 10
	},
	photo: {
		width: thumbWidth,
		height: thumbHeight,
		borderRadius: 5
	},
	textBold: {
		paddingBottom: 20,
		paddingTop: 20,
		fontSize: 20,
		textAlign: 'center',
		fontWeight: 'bold',
		fontFamily: 'GothamRounded-Bold'
	},
	textLabel: {
		fontSize: 14,
		fontFamily: 'GothamRounded-Medium'
	},
	textValue: {
		flexWrap: 'wrap',
		flex: 1,
		color: '#05C0FE',
		fontSize: 14,
		fontFamily: 'GothamRounded-Medium'
	},
	buttonMore: {
		backgroundColor: '#67a847',
		marginTop: 20,
		marginBottom: 10,
		padding: 7,
		borderRadius: 20,
		alignItems: 'center'
	},
	buttonText: {
		paddingTop: Platform.OS == 'ios' ? 4 : 0,
		color: colors.white,
		fontWeight: 'bold',
		fontSize: 16,
		fontFamily: 'GothamRounded-Bold'
	}
});

export default pageStyle;
