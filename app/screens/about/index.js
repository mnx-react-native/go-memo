import {
	View,
	Text,
	Image,
	Linking,
	ScrollView,
	TouchableOpacity,
	ImageBackground
} from 'react-native';
import React, { Component } from 'react';

import commonStyle from '../../common/style';
import pageStyle from './style';
import images from '../../common/image';

import Header from '../../common/header';
import Footer from '../../common/footer';

class AboutUs extends Component {
	constructor(props, context) {
		super(props, context);
		this.openLink = this.openLink.bind(this);
	}

	openLink() {
		Linking.openURL('http://www.akdn.org/');
	}

	onLeftPress() {
		this.props.navigation.goBack();
	}

	render() {
		return (
			<ImageBackground
				source={images.app.backgroundScreen2}
				style={commonStyle.backgroundImage}
			>
				<Header theme="light" title="About Us" onLeftPress={() => this.onLeftPress()} />

				<View style={commonStyle.container}>
					<ScrollView style={pageStyle.content}>
						<Text style={pageStyle.header}>Aga Khan Development Network</Text>
						<Text style={pageStyle.title}>Diamond Jubilee Commemorative</Text>
						<Text style={pageStyle.textDescription}>
							His Highness the Aga Khan, the founder and chairman of the AKDN, is the
							49th hereditary Imam (Spiritual Leader) of the Shia Ismaili Muslims. In
							Islam’s ethical tradition, religious leaders not only interpret the
							faith but also have a responsibility to help improve the quality of life
							in their community and in the societies amongst which they live. For His
							Highness the Aga Khan, this has meant a deep engagement with development
							for over 50 years through the agencies of the AKDN. As a descendant of
							the Fatimids, the Egypt-based dynasty that founded Cairo and ruled much
							of North Africa and the Middle East from the tenth through the twelfth
							centuries, the Aga Khan retains the hereditary title of “Prince”. The
							title of “Aga Khan” dates to 1818, when Hassan Ali Shah, the 46th
							Ismaili Imam, was granted the honorary hereditary title of “Aga Khan” by
							the Shah of Persia. The title “His Highness” was granted by Her Majesty
							the Queen of Great Britain in 1957. The agencies of the AKDN are
							private, international, non-denominational development organisations.
							They work to improve the welfare and prospects of people in the
							developing world, particularly in Asia and Africa. Some programmes, such
							as specific research, education and cultural programmes, span both the
							developed and developing worlds. While each agency pursues its own
							mandate, all of them work together within the overarching framework of
							the Network so that their different pursuits interact and reinforce one
							another.
						</Text>
						{images.about.map((credit, index) => {
							return (
								<View style={pageStyle.photoCover} key={index}>
									<Image source={credit.source} style={pageStyle.photo} />
									<View style={pageStyle.photoInfo}>
										<Text style={pageStyle.textLabel}>Name: </Text>
										<Text style={pageStyle.textValue}>{credit.name}</Text>
									</View>
									<View style={pageStyle.photoInfo}>
										<Text style={pageStyle.textLabel}>City: </Text>
										<Text style={pageStyle.textValue}>{credit.city}</Text>
									</View>
									<View style={pageStyle.photoInfo}>
										<Text style={pageStyle.textLabel}>Country: </Text>
										<Text style={pageStyle.textValue}>{credit.country}</Text>
									</View>
								</View>
							);
						})}
					</ScrollView>
					<TouchableOpacity onPress={this.openLink} activeOpacity={0.9}>
						<View style={pageStyle.buttonMore}>
							<Text style={pageStyle.buttonText}>Know more</Text>
						</View>
					</TouchableOpacity>
				</View>
				<Footer theme="white" />
			</ImageBackground>
		);
	}
}

export default AboutUs;
