const Level = {
	'level-1': {
		title: 'Level 1',
		size: 8,
		moves: 16,
		time: 120,
		layout: [[1, 1, 1], [1, 0, 1], [1, 1, 1]]
	},
	'level-2': {
		title: 'Level 2',
		size: 12,
		moves: 22,
		time: 150,
		layout: [[1, 1, 1], [1, 1, 1], [1, 1, 1], [1, 1, 1]]
	},
	'level-3': {
		title: 'Level 3',
		size: 14,
		moves: 28,
		time: 180,

		layout: [[1, 1, 1, 1], [1, 0, 1, 1], [1, 1, 0, 1], [1, 1, 1, 1]]
	},
	'level-4': {
		title: 'Level 4',
		size: 16,
		moves: 34,
		time: 210,
		layout: [[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]]
	},
	'level-5': {
		title: 'Level 5',
		size: 20,
		moves: 40,
		time: 240,
		layout: [[1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1], [1, 1, 1, 1]]
	}
};

export default Level;
