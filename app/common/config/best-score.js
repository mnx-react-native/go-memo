const BestScore = {
	'Easy:level-1': {
		move: 999,
		time: 999
	},
	'Easy:level-2': {
		move: 999,
		time: 999
	},
	'Easy:level-3': {
		move: 999,
		time: 999
	},
	'Easy:level-4': {
		move: 999,
		time: 999
	},
	'Easy:level-5': {
		move: 999,
		time: 999
	},

	'Medium:level-1': {
		move: 999,
		time: 999
	},
	'Medium:level-2': {
		move: 999,
		time: 999
	},
	'Medium:level-3': {
		move: 999,
		time: 999
	},
	'Medium:level-4': {
		move: 999,
		time: 999
	},
	'Medium:level-5': {
		move: 999,
		time: 999
	},

	'Hard:level-1': {
		move: 999,
		time: 999
	},
	'Hard:level-2': {
		move: 999,
		time: 999
	},
	'Hard:level-3': {
		move: 999,
		time: 999
	},
	'Hard:level-4': {
		move: 999,
		time: 999
	},
	'Hard:level-5': {
		move: 999,
		time: 999
	}
};

export default BestScore;
