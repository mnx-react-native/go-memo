const images = {
	app: {
		backgroundCard: require('../../images/app/background-card.png'),
		backgroundScreen: require('../../images/app/background-screen.png'),
		backgroundScreen2: require('../../images/app/background-screen2.png'),
		buttonMenu: require('../../images/app/button-menu.png'),
		buttonModal: require('../../images/app/button-modal.png'),
		buttonOption: require('../../images/app/button-option.png'),
		buttonSwitch: require('../../images/app/button-switch.png'),
		iconArrowDown: require('../../images/app/icon-arrow-down.png'),
		iconBackBlack: require('../../images/app/icon-back-black.png'),
		iconBackWhite: require('../../images/app/icon-back-white.png'),
		iconClose: require('../../images/app/icon-close.png'),
		iconOption: require('../../images/app/icon-option.png'),
		iconPlay: require('../../images/app/icon-play.png'),
		iconAbout: require('../../images/app/icon-about.png'),
		iconScore: require('../../images/app/icon-score.png'),
		inputCheck: require('../../images/app/input-check.png'),
		inputChecked: require('../../images/app/input-checked.png'),
		logoBrand: require('../../images/app/logo-brand.png')
	},
	about: [
		{
			country: 'Bangladesh',
			city: 'Dhaka',
			name: 'Bait Ur Rouf Mosque',
			source: require('../../images/about/akaa-2016-bangladesh.jpg')
		},
		{
			country: 'Afghanistan',
			city: 'Bamiyan',
			name: 'The new Bamyan Hospital',
			source: require('../../images/about/akf-afghanistan-dsc.jpg')
		},
		{
			country: 'Kyrgyzstan',
			city: 'Naryn',
			name: 'The Naryn Campus of the UCA',
			source: require('../../images/about/uca-kyrgyz-republic-naryn-ott2295.jpg')
		}
	],
	riddles: [
		{
			description: 'Aga Khan Academy, Mombasa, Kenya',
			name: 'Aga Khan Academy',
			location: 'Mombasa, Kenya',
			agency: 'Aga Khan Academies',
			credit: 'Gary Otte',
			url: 'http://www.agakhanacademies.org/mombasa',
			source: require('../../images/playing/Aga-Khan-Academy.png')
		},
		{
			description: 'Aga Khan Academy, Hyderabad, India',
			name: 'Aga Khan Academy',
			location: 'Hyderabad, India',
			agency: 'Aga Khan Academies',
			credit: 'Gary Otte',
			url: 'http://www.agakhanacademies.org/hyderabad',
			source: require('../../images/playing/Aga-Khan-Academy-Hyderabad.png')
		},
		{
			description:
				'Ranavav Semi-urban Day Care Centre: Supported by the Aga Khan Education Services since 1986, and managed entirely by a voluntary local management committee, the Ranavav semi-urban day care centre in Gujarat, India, offers quality pre-primary education to children from Muslim and other marginalized communities. 40% of enrollment is girls.',
			name: 'Ranavav Semi-urban Day Care Centre',
			location: 'Gujarat, India',
			agency: 'Aga Khan Education Services',
			credit: 'Jean-Luc Ray/AKDN',
			url: 'http://www.akdn.org/our-agencies/aga-khan-education-services',
			source: require('../../images/playing/Ranavav-Semi-urban-Day-Care-Centre.png')
		},
		{
			description:
				'Home Improvement Project: On the roof of a house in Mayoon, Pakistan. The owner has adopted a number of home improvement ideas put forth by the Aga Khan Agency for Habitat. This is a storm window covering for the traditional smoke hole. The window can be opened to let in fresh air in the summer, but it can be shut from the inside to keep out the cold.',
			name: 'Home Improvement Project',
			location: 'Mayoon, Pakistan',
			agency: 'Aga Khan Planning and Building Services',
			credit: 'Jean-Luc Ray/AKDN',
			url: 'http://www.akdn.org/our-agencies/aga-khan-agency-habitat',
			source: require('../../images/playing/Home-Improvement-Project.png')
		},
		{
			description:
				'Tamam Al Khoujey, a vendor from Syria, took out two Aga Khan Agency for Microfinance loans (in 2003 and 2007) to expand his shop. As a result, his monthly net income has nearly doubled.',
			name: 'Vendor in Masyaf',
			location: 'Masyaf, Syria',
			agency: 'Aga Khan Agency for Microfinance',
			credit: 'Jean-Luc Ray/AKF',
			url: 'http://www.akdn.org/our-agencies/aga-khan-agency-micro',
			source: require('../../images/playing/Vendor-in-Masyaf.png')
		},
		{
			description:
				'Aviation in Burkina Faso: The aim of the Aga Khan Fund for Economic Development (AKFED)’s Aviation Services division is to assist Burkina Faso in creating and maintaining critical aviation infrastructure in support of economic development. AKFED provides investment, expertise in airline and airport operations, as well as 	management and training for the sector.',
			name: 'Aviation in Burkina Faso',
			location: 'Ouagadougou,  Burkina Faso',
			agency: 'Aga Khan Fund for Economic Development (AKFED)',
			credit: 'Lucas Cuervo Moura',
			url:
				'http://www.akdn.org/our-agencies/aga-khan-fund-economic-development/akfed-companies',
			source: require('../../images/playing/Aviation-in-Burkina-Faso.png')
		},
		{
			description:
				"Bujagali Hydroelectric Project: The US$ 770 million Bujagali hydroelectric project, partly owned by the Aga Khan Fund for Economic Development (AKFED), is Uganda's first private hydroelectric power project, and is expected to significantly lower the price of electricity in Uganda. The plant is also one of the largest independent power plants in sub-Saharan Africa.",
			name: 'Bujagali Hydroelectric Project',
			location: 'Bujagali, Uganda',
			agency: 'Aga Khan Fund for Economic Development (AKFED)',
			credit: 'Zul Mukhida',
			url: 'http://www.akdn.org/our-agencies/aga-khan-fund-economic-development',
			source: require('../../images/playing/Bujagali-Hydroelectric-Project.png')
		},
		{
			description:
				'Uganda Fishnet Manufacturers Limited, a project company of the Aga Khan Fund for Economic Development (AKFED), provides a locally manufactured supply of quality fishnets, an essential product for one of Uganda’s most significant export industries.',
			name: 'Uganda Fishnet',
			location: 'Kampala, Uganda',
			agency: 'Aga Khan Fund for Economic Development (AKFED)',
			credit: 'Zul Mukhida',
			url: 'http://www.akdn.org/what-we-do/industrial-development',
			source: require('../../images/playing/Uganda-Fishnet.png')
		},
		{
			description:
				'A poultry farmer in Syria receives a home visit from a poultry lab staff member, to check on the health and growth of these 40-day chicks. A project of the Aga Khan Rural Support Program (AKRSP).',
			name: 'Poultry Farmer',
			location: 'Salamieh, Syria',
			agency: 'Aga Khan Rural Support Program (AKRSP)',
			credit: 'Jean-Luc Ray/AKF',
			url:
				'http://www.akdn.org/our-agencies/aga-khan-foundation/rural-development/rural-development-overview',
			source: require('../../images/playing/Poultry-Farmer.png')
		},
		{
			description:
				"Village in Tajikistan: Together with village organizations and as part of agricultural reform, the Aga Khan Foundation and the Mountain Societies Development Program run agriculture and livestock production programs to promote food security in mountainous areas and increase land and livestock productivity. A year ago, Tajikistan's Shirmui Muminova was a beneficiary of one goat. She now has eight goats, which she breeds for meat. After two years, she will return one goat to the village organization which will then grant it to a new beneficiary.",
			name: 'Village in Tajikistan',
			location: 'Gorno-Badakhshan, including the Rasht Valley, Sughd, and Khatlon',
			agency: 'Aga Khan Foundation and the Mountain Societies Development Program',
			credit: 'Jean-Luc Ray/AKF',
			url: 'http://www.akdn.org/where-we-work/central-asia/tajikistan/rural-development',
			source: require('../../images/playing/Village-in-Tajikistan.png')
		},
		{
			description:
				"Aga Khan Foundation USA's Milk for Hope program, that delivered milk to 50,000 school children in Tajikistan and Afghanistan",
			name: 'Milk and Hope',
			location: 'Afghanistan and Tajikistan',
			agency: 'Aga Khan Foundation USA',
			credit: 'Martha Sipple',
			url:
				'http://www.akdn.org/video/milk-and-hope-education-dairy-and-nutrition-programme-tajikistan',
			source: require('../../images/playing/Milk-and-Hope.png')
		},
		{
			description:
				'The Aga Khan University (AKU) Hospital in Nairobi, which became an AKU teaching hospital in 2005, will celebrate its 50th anniversary in 2008. It focuses on high quality of care, research and postgraduate medical education in all major clinical specialties. It was originally the Platinum Jubilee Hospital.',
			name: 'Aga Khan University Hospital (Originally Platinum Jubilee Hospital)',
			location: 'Nairobi, Kenya',
			agency: 'Aga Khan Health Services',
			credit: 'Jean-Luc Ray',
			url: 'https://hospitals.aku.edu/nairobi/AboutUs/Pages/OurProfile.aspx',
			source: require('../../images/playing/Aga-Khan-University-Hospital.png')
		},
		{
			description:
				'Citadel of Masyaf, Syria, a 12th century Ismaili fortress. Conservation work was done by the Aga Khan Trust for Culture (AKTC)',
			name: 'Citadel of Masyaf',
			location: 'Masyaf, Syria',
			agency: 'Aga Khan Trust for Culture',
			credit: 'Gary Otte',
			url: 'https://archnet.org/sites/6412/publications/6697',
			source: require('../../images/playing/Citadel-of-Masyaf.png')
		},
		{
			description:
				'Women working in their vegetable garden in Northern Pakistan One woman keeps a record of progress. She is an Aga Khan Rural Support Program (AKRSP)’s' +
				'“Women in Development”' +
				"staff member who encourages formation of Women's Organizations.",
			name: 'Women tending a vegetable garden',
			location: 'Northern Pakistan',
			agency: 'Aga Khan Rural Support Program (AKRSP)',
			credit: 'Jean Mohr',
			url: 'http://www.akdn.org/where-we-work/south-asia/pakistan/rural-development',
			source: require('../../images/playing/Women-tending-a-vegetable-garden.png')
		},
		{
			description:
				'Health Clinic in Cabo Delgado Province: Improving overall health status is one of the priorities of the Aga Khan Foundation (AKF)’s Coastal Rural Support Program in Mozambique. Particular emphasis has been placed on improving the health of children under five and women of childbearing age, with vaccination and bed net campaigns contributing to decreasing the prevalence of diseases such as measles and malaria.',
			name: 'Cabo Delgado Province',
			location: 'Mozambique',
			agency: 'Aga Khan Foundation',
			credit: 'Lucas Cuervo Moura / AKDN',
			url: 'http://www.akdn.org/where-we-work/eastern-africa/mozambique/health',
			source: require('../../images/playing/Cabo-Delgado-Province.png')
		},
		{
			description: 'A winter view of The Ismaili Centre, Burnaby, from the south-west',
			name: 'The Ismaili Centre, Burnaby',
			location: 'Burnaby, British Columbia, Canada',
			agency: 'Assembly Spaces',
			credit: 'Gary Otte',
			url: 'https://the.ismaili/burnaby/about-burnaby',
			source: require('../../images/playing/The-Ismaili-Centre-Burnaby.png')
		},
		{
			description: 'Night view of the entrance of The Ismaili Centre, Dubai',
			name: 'The Ismaili Centre, Dubai',
			location: 'Dubai, United Arab Emirates',
			agency: 'Assembly Spaces',
			credit: 'Gary Otte',
			url: 'https://the.ismaili/ismailicentres/dubai',
			source: require('../../images/playing/The-Ismaili-Centre-Dubai.png')
		},
		{
			description: 'Inner courtyard with the water channel, The Ismaili Centre, Lisbon',
			name: 'The Ismaili Centre, Lisbon',
			location: 'Lisbon, Portugal',
			agency: 'Assembly Spaces',
			credit: 'Gary Otte',
			url: 'https://the.ismaili/ismailicentres/lisbon',
			source: require('../../images/playing/The-Ismaili-Centre-Lisbon.png')
		},
		{
			description: 'Roof garden of the The Ismaili Centre, London, United Kingdom',
			name: 'The Ismaili Centre, London',
			location: 'London, United Kingdom',
			agency: 'Assembly Spaces',
			credit: 'Gary Otte',
			url: 'https://the.ismaili/ismailicentres/london',
			source: require('../../images/playing/The-Ismaili-Centre-London.png')
		},
		{
			description: 'The Ismaili Centre, Dushanbe, Tajikistan',
			name: 'The Ismaili Centre, Dushanbe',
			location: 'Dushanbe, Tajikistan',
			agency: 'Assembly Spaces',
			credit: 'Gary Otte',
			url: 'https://the.ismaili/ismailicentres/dushanbe',
			source: require('../../images/playing/The-Ismaili-Centre-Dushanbe.png')
		},
		{
			description: 'Crystalline dome of The Ismaili Centre, Toronto, Canada, at dusk',
			name: 'The Ismaili Centre, Toronto',
			location: 'Toronto, Canada',
			agency: 'Assembly Spaces',
			credit: 'Gary Otte',
			url: 'https://the.ismaili/ismailicentres/toronto',
			source: require('../../images/playing/The-Ismaili-Centre-Toronto.png')
		},
		{
			description:
				'Sandbag Shelters in Ahwaz, Iran: Winner of the 2004 Aga Khan Award for Architecture, this sandbag shelter prototype uses packed earth and barbed wire to create a solid and cheap structure for families.',
			name: 'Sandbag Shelters',
			location: 'Ahwaz, Iran',
			agency: 'Aga Khan Trust for Culture',
			credit: 'Cal-Earth Institute',
			url: 'http://www.akdn.org/architecture/project/sandbag-shelters',
			source: require('../../images/playing/Sandbag-Shelters.png')
		},
		{
			description:
				'Restoration of Al Aqsa Mosque, Jerusalem, winner of the 1986 Aga Khan Award for Architecture',
			name: 'Al Aqsa Mosque',
			location: 'Jerusalem',
			agency: 'Aga Khan Trust for Culture',
			credit: 'AKTC',
			url: 'http://www.akdn.org/architecture/project/restoration-of-al-aqsa-mosque',
			source: require('../../images/playing/Al-Aqsa-Mosque.png')
		},
		{
			description:
				'Petronas Towers, Kuala Lumpur, Malaysia, winner of the 2004 Aga Khan Award for Architecture',
			name: 'Petronas Towers',
			location: 'Kuala Lumpur, Malaysia',
			agency: 'Aga Khan Trust for Culture',
			credit: 'AKTC',
			url: 'http://www.akdn.org/architecture/project/petronas-office-towers',
			source: require('../../images/playing/Petronas-Towers.png')
		},
		{
			description:
				"Bardic Divas; Women's Voices in Central Asia:Cover of the CD music anthology, Bardic Divas, featuring the music of Central Asia, produced by the Aga Khan Music Initiative",
			name: "Bardic Divas: Women's voices in Central Asia",
			location: 'Khorezm region of Uzbekistan',
			agency: 'Aga Khan Trust for Culture and the Aga Khan Music Initiative',
			credit: 'Theodore Levin',
			url:
				'http://www.akdn.org/our-agencies/aga-khan-trust-culture/akmi/cd-dvd-series/bardic-divas',
			source: require('../../images/playing/Women-voices-in-Central-Asia.png')
		},
		{
			description:
				'The Nation Centre, Nairobi, Kenya; headquarters of the Nation Media Group',
			name: 'Nation Media',
			location: 'Nairobi, Kenya',
			agency: 'Aga Khan Fund for Economic Development (AKFED)',
			credit: 'unknown',
			url:
				'http://www.akdn.org/our-agencies/aga-khan-fund-economic-development/media-services',
			source: require('../../images/playing/Nation-Media.png')
		},
		{
			description:
				'National Assembly Building, Dhaka, Bangladesh. Winner of the 1989 Aga Khan Award for Architecture',
			name: 'National Assembly Building',
			location: 'Dhaka, Bangladesh',
			agency: 'Aga Khan Trust for Culture',
			credit: 'Abu H. Imamuddin (AKTC)',
			url: 'http://www.akdn.org/gallery/1989-aga-khan-award-architecture-recipients',
			source: require('../../images/playing/National-Assembly-Building.png')
		},
		{
			description:
				'Kaedi Regional Hospital, Kaedi, Mauritania Winner of the 1995 Aga Khan Award for Architecture',
			name: 'Kaedi Regional Hospital',
			location: 'Kaedi, Mauritania',
			agency: 'Aga Khan Trust for Culture',
			credit: 'AKTC',
			url: 'http://www.akdn.org/architecture/project/kaedi-regional-hospital',
			source: require('../../images/playing/Kaedi-Regional-Hospital.png')
		},
		{
			description:
				'The Hajj Terminal, Jeddah, Saudi Arabia Winner of the 1983 Aga Khan Award for Architecture',
			name: 'Hajj Terminal',
			location: 'Jeddah, Saudi Arabia',
			agency: 'Aga Khan Trust for Culture',
			credit: 'AKTC',
			url: 'http://www.akdn.org/architecture/project/hajj-terminal',
			source: require('../../images/playing/Hajj-Terminal.png')
		},
		{
			description: 'Kuwait Water Towers Winner of the 1980 Aga Khan Award for Architecture',
			name: 'Kuwait Water Towers',
			location: 'Kuwait City, Kuwait',
			agency: 'Aga Khan Trust for Culture',
			credit: 'AKTC',
			url: 'http://www.akdn.org/architecture/project/water-towers',
			source: require('../../images/playing/Kuwait-Water-Towers.png')
		},
		{
			description: 'Building of the Delegation of the Ismaili Imamat, Ottawa, Canada',
			name: 'Delegation of the Ismaili Imamat',
			location: 'Ottawa, Canada',
			agency: 'Aga Khan Foundation',
			credit: 'Gary Otte',
			url: 'https://www.akfc.ca/about-us/visit-us/',
			source: require('../../images/playing/Delegation-of-the-Ismaili-Imamat.png')
		},
		{
			description: 'The Aga Khan Museum, Toronto, Canada',
			name: 'Aga Khan Museum',
			location: 'Toronto, Canada',
			agency: 'Aga Khan Trust for Culture',
			credit: 'Janet Kimber',
			url: 'https://www.agakhanmuseum.org',
			source: require('../../images/playing/Aga-Khan-Museum.png')
		},
		{
			description:
				'The restoration of the Great Mosque of Mopti, in Djenne, Mali, was undertaken by the Aga Khan Trust for Culture (AKTC) between 2004 and 2006. The Mosque, commonly called the Mosque of Komoguel, was at risk of collapse',
			name: 'Great Mosque of Mopti',
			location: 'Djenne, Mali',
			agency: 'Aga Khan Trust for Culture',
			credit: 'Ibai Rigby',
			url: 'https://archnet.org/sites/3465/publications/2868',
			source: require('../../images/playing/Great-Mosque-of-Mopti.png')
		},
		{
			description:
				'In Syria, the Directorate of Antiquities requested the Historic Cities Program to provide technical assistance and training for the conservation and management of three major citadels in Aleppo, Masyaf and Qalat Salah ed-Din',
			name: 'Citadel of Aleppo',
			location: 'Aleppo, Syria',
			agency: 'Aga Khan Trust for Culture',
			credit: 'Gary Otte',
			url: 'http://www.akdn.org/press-release/aga-khan-inaugurates-aleppo-citadel-project',
			source: require('../../images/playing/Citadel-of-Aleppo.png')
		},
		{
			description: 'The Global Centre for Pluralism, Ottawa, Canada',
			name: 'Global Centre for Pluralism',
			location: 'Ottawa, Canada',
			agency: 'AKDN',
			credit: 'Marc Fowler',
			url: 'http://www.akdn.org/where-we-work/north-america/canada/global-centre-pluralism',
			source: require('../../images/playing/Global-Centre-for-Pluralism.png')
		},
		{
			description:
				'Aerial View of the Naryn Campus, University of Central Asia, Naryn, Kyrgyzstan',
			name: 'Naryn Campus, University of Central Asia',
			location: 'Naryn, Kyrgyzstan',
			agency: 'University of Central Asia',
			credit: 'Emil Akhmatbekov',
			url: 'http://www.ucentralasia.org',
			source: require('../../images/playing/Naryn-Campus-University-of-Central-Asia.png')
		}
	]
};

images.riddles.forEach((photo, index) => {
	photo.id = index;
});

export default images;
