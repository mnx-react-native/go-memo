import { Text, ImageBackground, Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import images from '../image';
import colors from '../style/color';

const pageStyle = StyleSheet.create({
	coverButton: {
		flexDirection: 'row',
		alignItems: 'center',
		alignContent: 'center'
	},
	button: {
		width: 20,
		height: 20,
		marginRight: 20
	},
	label: {
		color: colors.white,
		fontSize: 16,
		paddingTop: Platform.OS === 'ios' ? 4 : 0,
		backgroundColor: 'transparent',
		fontFamily: 'GothamRounded-Bold'
	}
});

const RadioButton = props => {
	return (
		<TouchableOpacity onPress={props.onPress} activeOpacity={1}>
			<View style={pageStyle.coverButton}>
				<ImageBackground
					style={pageStyle.button}
					source={images.app[props.checked ? 'inputChecked' : 'inputCheck']}
				/>
				<Text style={pageStyle.label}>{props.label}</Text>
			</View>
		</TouchableOpacity>
	);
};

RadioButton.propTypes = {
	label: PropTypes.string,
	checked: PropTypes.bool,
	onPress: PropTypes.func
};

export default RadioButton;
