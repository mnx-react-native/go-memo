import {
	Text,
	View,
	Image,
	Platform,
	StyleSheet,
	TouchableOpacity,
	ImageBackground
} from 'react-native';
import React from 'react';
import images from '../image';

import colors from '../style/color';
import PropTypes from 'prop-types';

const pageStyle = StyleSheet.create({
	coverButton: {
		width: 157,
		height: 45,
		alignItems: 'center',
		flexDirection: 'row'
	},
	switchButton: {
		flexDirection: 'row',
		borderRadius: 22,
		alignItems: 'center',
		width: 79,
		height: 44,
		position: 'absolute'
	},
	switchOn: {
		position: 'absolute',
		backgroundColor: 'transparent',
		left: 26,
		paddingTop: Platform.OS === 'ios' ? 4 : 0,
		fontFamily: 'GothamRounded-Bold'
	},
	switchOff: {
		position: 'absolute',
		backgroundColor: 'transparent',
		right: 26,
		paddingTop: Platform.OS === 'ios' ? 4 : 0,
		fontFamily: 'GothamRounded-Bold'
	}
});

const SwitchButton = props => {
	const value = props.value;
	return (
		<TouchableOpacity onPress={props.onPress} activeOpacity={1}>
			<ImageBackground source={images.app.buttonOption} style={pageStyle.coverButton}>
				<Image
					source={images.app.buttonSwitch}
					style={[pageStyle.switchButton, { [value == true ? 'left' : 'right']: 0 }]}
				/>
				<Text style={[pageStyle.switchOn, { color: value == true ? 'white' : 'black' }]}>
					{props.labelOn}
				</Text>
				<Text style={[pageStyle.switchOff, { color: value == true ? 'black' : 'white' }]}>
					{props.labelOff}
				</Text>
			</ImageBackground>
		</TouchableOpacity>
	);
};

SwitchButton.propTypes = {
	onPress: PropTypes.func,
	value: PropTypes.bool,
	labelOn: PropTypes.string,
	labelOff: PropTypes.string
};

export default SwitchButton;
