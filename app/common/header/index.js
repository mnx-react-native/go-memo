import { Text, View, Image, Platform, StyleSheet, TouchableOpacity } from 'react-native';
import React from 'react';
import colors from '../style/color';
import images from '../../common/image';

import PropTypes from 'prop-types';

const boldText = {
	fontWeight: 'bold',
	fontFamily: 'GothamRounded-Bold'
};

const pageStyle = StyleSheet.create({
	header: {
		paddingTop: 20,
		width: null,
		flexDirection: 'row'
	},
	coverLeft: {
		position: 'absolute',
		width: 60,
		left: 0,
		bottom: 0,
		height: 26,
		alignItems: 'center',
		backgroundColor: 'transparent'
	},
	coverTitle: {
		flex: 1,
		marginLeft: 60,
		marginRight: 60,
		alignItems: 'center',
		backgroundColor: 'transparent'
	},
	coverRight: {
		position: 'absolute',
		width: 60,
		right: 0,
		bottom: 0,
		height: 26,
		alignItems: 'center',
		backgroundColor: 'transparent'
	},
	arrowIcon: {
		height: 18
	},
	darkTitle: {
		...boldText,
		fontSize: 22,
		color: colors.jet
	},
	lightTitle: {
		...boldText,
		fontSize: 22,
		color: colors.whiteSmoke
	},
	darkLabel: {
		...boldText,
		fontSize: 16,
		color: colors.jet
	},
	lightLabel: {
		...boldText,
		fontSize: 16,
		color: colors.whiteSmoke
	}
});

const Header = props => {
	const theme = props.theme;
	const pageTitle = theme == 'dark' ? pageStyle.darkTitle : pageStyle.lightTitle;
	const rightLabel = theme == 'dark' ? pageStyle.darkLabel : pageStyle.lightLabel;
	return (
		<View style={pageStyle.header}>
			<View style={pageStyle.coverLeft}>
				{props.onLeftPress != null ? (
					<TouchableOpacity onPress={props.onLeftPress}>
						<Image
							resizeMode="contain"
							style={pageStyle.arrowIcon}
							source={images.app[theme == 'dark' ? 'iconBackBlack' : 'iconBackWhite']}
						/>
					</TouchableOpacity>
				) : null}
			</View>
			<View style={pageStyle.coverTitle}>
				<Text style={pageTitle}>{props.title}</Text>
			</View>
			<View style={pageStyle.coverRight}>
				{props.onRightPress != null ? (
					<TouchableOpacity onPress={props.onRightPress}>
						<Text style={rightLabel}>{props.rightLabel}</Text>
					</TouchableOpacity>
				) : null}
			</View>
		</View>
	);
};

Header.propTypes = {
	theme: PropTypes.string,
	title: PropTypes.string,
	rightLabel: PropTypes.string,
	onLeftPress: PropTypes.func,
	onRightPress: PropTypes.func
};

export default Header;
