import { Text, View, StyleSheet } from 'react-native';
import React from 'react';
import colors from '../style/color';
import PropTypes from 'prop-types';

const pageStyle = StyleSheet.create({
	footer: {
		justifyContent: 'center',
		alignItems: 'center',
		width: null,
		paddingTop: 5,
		paddingBottom: 15
	},
	darkText: {
		color: colors.jet,
		backgroundColor: 'transparent'
	},
	lightText: {
		color: colors.whiteSmoke,
		backgroundColor: 'transparent'
	}
});

const Footer = props => {
	const textTheme = props.theme == 'dark' ? pageStyle.darkText : pageStyle.lightText;
	return (
		<View style={pageStyle.footer}>
			<Text style={textTheme}>@ Copyright 2017. All rights resereved.</Text>
		</View>
	);
};

Footer.propTypes = {
	theme: PropTypes.string
};

export default Footer;
