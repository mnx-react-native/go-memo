import { StyleSheet } from 'react-native';
import colors from './color';

const pageStyle = StyleSheet.create({
	backgroundImage: {
		flex: 1,
		width: null,
		height: null
		// resizeMode: 'cover'
	},
	container: {
		flex: 1,
		paddingLeft: 15,
		paddingRight: 15,
		flexDirection: 'column'
	}
});

export default pageStyle;
